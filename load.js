var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', {
            font: '30px Arial', fill: '#ffffff' 
        });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'loading');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // // Load all game assets
        // game.load.image('player', 'assets/player.png');
        // game.load.image('enemy', 'assets/enemy.png');
        // game.load.image('coin', 'assets/coin.png');
        // game.load.image('wallV', 'assets/wallVertical.png');
        // game.load.image('wallH', 'assets/wallHorizontal.png');
        // // Load a new asset that we will use in the menu state
        // game.load.image('background', 'assets/background.png');

        game.load.image('bullet', 'pictures/bullet.png');
        game.load.image('enemyBullet', 'pictures/enemy-bullet.png');
        game.load.spritesheet('invader', 'pictures/invader32x32x4.png', 32, 32);
        game.load.spritesheet('ship', 'pictures/ship4.png',30,25);
        game.load.spritesheet('kaboom', 'pictures/explode.png', 128, 128);
        game.load.image('starfield', 'pictures/starfield.png');
        game.load.image('background', 'pictures/bg1.png');
        game.load.image('pixel', 'pictures/pixel.png');
        game.load.image('ship1', 'pictures/player.png');
    },
    create: function() {
    // Go to the menu state
        game.state.start('menu');
    }
}; 