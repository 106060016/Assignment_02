# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 106060016_as02
* Key functions (add/delete)
    1. 左右移動
    2. 子彈碰到敵人
    3. 敵人子彈碰到自己
    4. 不同的難度
    5. 有兩種配樂
    6. player 跟enemy都有動畫
    7. 有UI
    
* Other functions (add/delete)
    1. 降低子彈速度
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|JUcify|15%|Y|
|Animation|15%|Y|
|Praticle|10%|Y|
|UI|5%|Y|
|sound|10%|Y|
|leader board|15%|N|
|Apperance|5%|Y|

## Bonus Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Bonus|40%|N|

# Components Description : 
1. 有boot,load,menu跟play，並且用game.js接起來
2. 有player,enemy，且當雙方子彈碰到對方會有判定
3. 有不同的難度，當打完第一輪的enemy之後，enemy子彈射速變快，cd變短且血量變多，但是player用過的大招跟血量不會增加，而player的大招是可以清除所有敵人的子彈，在後面便很難的關卡中有很大的功用，當按下V就是大招，不過有限制只能使用3次。
4. player跟enemy都有動畫
5. 有praticle system，有UI顯示
6. 有兩種以上的音量，有背景音樂也又打到敵人時的音效。(背景音樂要等一小段時間才會有聲音，大概5秒
7. 有暫停的功能
8. 遊戲結束後可以選擇要跳到主畫面還是再來一次

# Other Functions Description(1~10%) : 
1. 當我按下z的時候，可以把敵人子彈的速度減半，可以提高操作的精細度
...