var menuState = {
    preload:function(){
        game.load.audio('bgm', 'musics/bgm.mp3');
    },

    create: function() {
        // Add a background image
        game.add.image(0, 0, 'background');
        //console.log("cre_menu");
        this.bgm = this.add.audio('bgm', 0.3, true);
        this.bgm.play();
        // Display the name of the game

        // var nameLabel = game.add.text(game.width/2, 80, 'Raiden',{ 
        //     font: '50px Arial', fill: '#ffffff' 
        // });
        // nameLabel.anchor.setTo(0.5, 0.5);

        // Show the score at the center of the screen
        
        // var scoreLabel = game.add.text(game.width/2, game.height/2,'score: ' + game.global.score, { 
        //     font: '25px Arial', fill: '#ffffff' 
        // });
        // scoreLabel.anchor.setTo(0.5, 0.5);

        // Explain how to start the game
        var startLabel = game.add.text(game.width/2, game.height-80,'press Space to start', { 
            font: '40px Arial', fill: '#ffffff' 
        });
        startLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upKey.onDown.add(this.start, this);
    },
    start: function() {
    // Start the actual game
        this.bgm.stop();
        game.state.start('play');
    },
}; 