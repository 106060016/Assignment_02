
//var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-canvas', { preload: preload, create: create, update: update, render: render });
var player;
var aliens;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var bigskill;
var cdbigskill=0;
var pause;
var pausemusic;
var resumemusic;
var controlspeed=0;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var skills;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var cnt=0;
var power;
var cntpause=0;
var cntspeed=0;
var upspeed;
var downspeed;
var lefspeed;
var rightspeed;
var auto;
var ebs=100;
var ebstmp=0;
var ebs1=100;
var pausetmp=250;
var cd=0;
var cdpause=0;
var cdmusic=0;
var cdmusic1=0;
var cdhitplayer=0;
var cntover=1;
var cdtext=0;
var cntnext=0;
var cdexplode=0;
var callhelper;
var cdhelper=0;
var cdauto=0;
var autotmp=0;

var playState={

    preload: function() {
        game.load.audio('bgm1', 'musics/playbgm.mp3');
        game.load.audio('bgm2', 'musics/explode.mp3');
        console.log("playstate");
    },
    
    create: function() {
        this.play_bgm = this.add.audio('bgm1', 0.3, true);
        this.play_bgm.play();
        this.explode_bgm = this.add.audio('bgm2', 0.3, true);
        //this.play_bgm.play();

        game.physics.startSystem(Phaser.Physics.ARCADE);
    
        //  The scrolling starfield background
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
        //game.paused = true
        //  Our bullet group
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(30, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);
    
        // The enemy's bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(30, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);
    
        //emitter
        
        //  The hero!
        // player = game.add.sprite(400, 500, 'ship');
        // player.anchor.setTo(0.5, 0.5);
        player = game.add.sprite(400, 500, 'ship');
        player.anchor.setTo(0.5, 0.5);
        player.animations.add('fly', [ 0, 1, 2, 3,4,5,6,7,8 ], 20, true);
        player.play('fly');
        game.physics.enable(player, Phaser.Physics.ARCADE);
    
        //  The baddies!
        aliens = game.add.group();
        aliens.enableBody = true;
        aliens.physicsBodyType = Phaser.Physics.ARCADE;
    
        this.createAliens();
    
        //  The score
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });
    
        //  Lives
        lives = game.add.group();
        game.add.text(game.world.width - 100, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });
        //大招
        skills = game.add.group();
        game.add.text(game.world.width - 100, 70, 'Skills : ', { font: '34px Arial', fill: '#fff' });

        // autos = game.add.group();
        // game.add.text(game.world.width - 100, 130, 'autos : ', { font: '34px Arial', fill: '#fff' });
    
        //  Text
        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;
    
        for (var i = 0; i < 3; i++) 
        {
            var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'ship');
            ship.anchor.setTo(0.5, 0.5);
            ship.angle = 90;
            ship.alpha = 0.4;
        }
        for (var i = 0; i < 3; i++) 
        {
            var ship1 = skills.create(game.world.width - 100 + (30 * i), 120, 'enemyBullet');
            ship1.anchor.setTo(0.5, 0.5);
            ship1.angle = 90;
            ship1.alpha = 0.4;
        }
    
        //  An explosion pool
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(this.setupInvader, this);
    
        //  And some controls to play the game with
        cursors = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        loudmusic = game.input.keyboard.addKey(Phaser.Keyboard.E);
        lowmusic = game.input.keyboard.addKey(Phaser.Keyboard.R);
        pausemusic = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        resumemusic = game.input.keyboard.addKey(Phaser.Keyboard.W);
        pause = game.input.keyboard.addKey(Phaser.Keyboard.A);
        pause.onDown.add(this.continue,self);
        bigskill =game.input.keyboard.addKey(Phaser.Keyboard.V);
        callhelper = game.input.keyboard.addKey(Phaser.Keyboard.X);
        auto = game.input.keyboard.addKey(Phaser.Keyboard.C);
        //bigskill.onDown.add(this.useskill,self);
        controlspeed = game.input.keyboard.addKey(Phaser.Keyboard.Z);    
    },
    continue:function(){
        if(game.paused==true){
            stateText.visible = false;
            game.paused=false;
        }
    },

    createAliens: function () {
    
        for (var y = 0; y < 4; y++)
        {
            for (var x = 0; x < 10; x++)
            {
                var alien = aliens.create(x * 48, y * 50, 'invader');
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.moves = false;
                alien.live=cntover;
            }
        }
    
        aliens.x = 100;
        aliens.y = 50;
    
        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    
        //  When the tween loops it calls descend
        tween.onLoop.add(this.descend, this);
    },
    
    setupInvader:function (invader) {
    
        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    
    },
    
    descend:function() {
    
        aliens.y += 10;
    
    },
    
    update:function() {

        //  Scroll the background
        starfield.tilePosition.y += 2;

        if(game.time.now>cdexplode){
            this.explode_bgm.stop();
        }
        if(pausemusic.isDown){
            this.play_bgm.pause();
            console.log("Q");
        }
        if(resumemusic.isDown){
            this.play_bgm.resume();
        }
        if(loudmusic.isDown){
            console.log(this.play_bgm.volume);
            if(game.time.now>cdmusic){
                if(this.play_bgm.volume<3){
                    this.play_bgm.volume+=0.3;
                }
                cdmusic=game.time.now+300;
            }
        }
        if(lowmusic.isDown){
            console.log(this.play_bgm.volume);
            if(game.time.now>cdmusic1){
                if(this.play_bgm.volume>0){
                    this.play_bgm.volume-=0.3;
                }
                cdmusic1=game.time.now+300;
            }
        }
        if(pause.isDown){
            if(game.time.now>=cdpause){
                console.log("pause");
                //pausetmp+=1;
                if(game.paused==false){
                    stateText.text = " Pause, \n Press a to resume";
                    stateText.visible = true;
                    game.paused=true;
                }
                else if(game.paused==true){
                    stateText.visible = false;
                    game.paused=false;
                    // console.log("A");
                    // console.log(game.time.now);
                    // console.log(cdpause);
                    cdpause=game.time.now+1000;
                }
            }
        }
        if(game.time.now>cdtext&&cntnext==1){
            stateText.visible = false;
            cntnext=0;
        }

        if (player.alive){
            this.move();
            // if(callhelper.isDown){
            //     if(game.time.now>cdhelper){
            //         cdhelper=game.time.now+1000;
            //         this.help();
            //     }
            // }
            if(bigskill.isDown){
                if(game.time.now>cdbigskill){
                    cdbigskill=game.time.now+1000;
                    this.useskill();
                }
            }
            if(controlspeed.isDown){
                if(game.time.now>cd){
                    console.log("cd");
                    ebstmp+=1;
                    cd=game.time.now+1000;
                    if(ebstmp%2==1){
                        ebs=ebs/2;
                    }
                    else{
                        ebs=ebs1;
                    }
                }
            }
            //  Firing?
            // if(auto.isDown){
            //     console.log("c");
            //     if(game.time.now>cdauto){
            //         console.log("c1");
            //         auto=autos.getFirstAlive();
            //         if(auto){
            //             auto.kill();
            //         }
            //         cdauto=game.time.now+1000;
            //         autotmp+=1;
            //     }
            // }
            if (fireButton.isDown){
                this.fireBullet();
            }
    
            if (game.time.now > firingTimer){
                this.enemyFires();
            }
    
            //  Run collision
            game.physics.arcade.overlap(bullets, aliens, this.collisionHandler, null, this);
            game.physics.arcade.overlap(enemyBullets, player,this.enemyHitsPlayer, null, this);
            game.physics.arcade.overlap(aliens, player,this.enemyHitsPlayer1, null, this);
            //game.physics.arcade.collide(bullets,aliens,this.blockParticle,null,this);
        }
    
    },
    // help:function(player){
    //     var help1 =game.add(player.body.x,player.body.y, 'ship1');
    //     help1.anchor.setTo(0.5, 0.5);
    // },
    move:function(){
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);
    
        if (cursors.left.isDown){
            player.body.velocity.x = -200;
        }
        else if (cursors.up.isDown){
            player.body.velocity.y = -150;
        }
        else if (cursors.down.isDown){
            player.body.velocity.y = 150;
        }
        else if (cursors.right.isDown){
            player.body.velocity.x = 200;
        }
    },

    render:function() {
    
        // for (var i = 0; i < aliens.length; i++)
        // {
        //     game.debug.body(aliens.children[i]);
        // }
    
    },
    
    collisionHandler:function (bullet, alien) {
        console.log(alien.live);
        alien.live-=1;
        bullet.kill();
        //  When a bullet hits an alien we kill them both
        if(alien.live==0){
            //  Increase the score
            score += 20;
            scoreText.text = scoreString + score;
            alien.kill();
            //  And create an explosion :)
            this.explode_bgm.play();
            cdexplode=game.time.now+3000;
            this.emitter = game.add.emitter(alien.body.x, alien.body.y, 15);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;
            this.emitter.start(true,800,null,15);
            // var explosion = explosions.getFirstExists(false);
            // explosion.reset(alien.body.x, alien.body.y);
            // explosion.play('kaboom', 30, false, true);
        }
    
    
        if (aliens.countLiving() == 0){
            cntover+=1;
            if(cntover==5){
                score += 1000;
                scoreText.text = scoreString + score;
        
                enemyBullets.callAll('kill');
                bullets.callAll('kill');
                stateText.text = " You Won, \n Click to restart \n Press M to menu";
                stateText.visible = true;
        
                //the "click to restart" handler
                var upKey = game.input.keyboard.addKey(Phaser.Keyboard.M);
                upKey.onDown.add(this.backtomenu, this);
                game.input.onTap.addOnce(this.restart,this);
            }
            score += 1000;
            scoreText.text = scoreString + score;
    
            enemyBullets.callAll('kill');
            bullets.callAll('kill');
            stateText.text = " You Won, \n Lets go Level"+cntover;
            ebstmp=0;
            ebs=ebs1;
            ebs1=ebs1+100;
            ebs=ebs+100;
            cntnext=1;
            stateText.visible = true;
            cdtext=game.time.now+1000;
            console.log(ebs);
            console.log(ebs1);
            aliens.removeAll();
            this.createAliens();
        
            //revives the player
            player.revive();
            //hides the text
        }
    
    },
    useskill:function(){
        console.log("useskill");
        skill=skills.getFirstAlive();
        if(skill){
            skill.kill();
            enemyBullets.callAll('kill');
        }
    },

    enemyHitsPlayer:function  (player,bullet) {
        
        bullet.kill();
    
        live = lives.getFirstAlive();
    
        if (live){
            live.kill();
        }
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            enemyBullets.callAll('kill');
    
            stateText.text=" GAME OVER \n Click to restart \n Press M to menu";
            stateText.visible = true;
    
            //the "click to restart" handler
            var upKey = game.input.keyboard.addKey(Phaser.Keyboard.M);
            upKey.onDown.add(this.backtomenu, this);
            game.input.onTap.addOnce(this.restart,this);
        }
    
    },
    enemyHitsPlayer1:function  (player,bullet) {
        
    
        live = lives.getFirstAlive();
        if (live&&game.time.now>cdhitplayer){
            cdhitplayer=game.time.now+3000;
            live.kill();
        }
    
        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            enemyBullets.callAll('kill');
    
            stateText.text=" GAME OVER \n Click to restart \nPress M to menu";
            stateText.visible = true;
    
            //the "click to restart" handler
            var upKey = game.input.keyboard.addKey(Phaser.Keyboard.M);
            upKey.onDown.add(this.backtomenu, this);
            game.input.onTap.addOnce(this.restart,this);
        }
    
    },
    
    enemyFires:function  () {
    
        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);
    
        livingEnemies.length=0;
    
        aliens.forEachAlive(function(alien){
    
            // put every living enemy in an array
            livingEnemies.push(alien);
        });
    
    
        if (enemyBullet && livingEnemies.length > 0)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
    
            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet,player,ebs);
            firingTimer = game.time.now + 300;
        }
    
    },
    
    fireBullet:function  () {
    
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);
            //console.log(autotmp);
            if (bullet){
                //  And fire it
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
            }
            // if(autotmp%2==1){
            //     bullet.reset(player.x, player.y + 8);
            //     game.physics.arcade.moveToObject(bullet,aliens,400);
            // }
        }
    },
    
    resetBullet :function(bullet) {
    
        //  Called if the bullet goes out of the screen
        bullet.kill();
    
    },
    
    restart :function () {
    
        //  A new level starts
        score=0;
        ebs=100;
        ebs1=100;
        cntover=1;
        //resets the life count
        lives.callAll('revive');
        skills.callAll('revive');
        //  And brings the aliens back from the dead :)
        aliens.removeAll();
        this.createAliens();
    
        //revives the player
        player.revive();
        //hides the text
        stateText.visible = false;
    
    },
    backtomenu: function() {
        // Go to the menu state
        score=0;
        this.play_bgm.stop();
        game.state.start('menu');
    }
};
